<?php
public function identify($account, $password, $password1)
{
	//如果添加$符号，则启用本地账号。
	if (0 == strcmp('$',substr($account, 0, 1))) {
		return parent::identify(ltrim($account, '$'), $password);
	} else {
		//进行LDAP用户验证
		$ldap = $this->loadModel('ldap');
		$dn = $ldap->getUserDN($this->config->ldap, $account);
                $pass = $ldap->identify($this->config->ldap, $dn, $password);
		if ('Success' == $pass){
			//验证成功
			//账户是否存在于禅道DB
			$data = $ldap->getUserMessageFromLDAP($this->config->ldap,$this->config->ldap->uid.'='.$account);
			if ($ldap->isExistInZenTaoDB($account)){
				//存在，更新用户信息并登录
				$email = $data[0][$this->config->ldap->mail][0];
				$ldap->updateUserDB($account, $password, $email);
				return parent::identify($account, $password);
			} else {
				//不存在，添加到禅道DB后进行登录
				$addUser = $ldap->addUserToZrnTaoDB($this->config->ldap, $data, $password);
				//设置用户默认权限
                $ldap->setDefaultUserGroup($account);
				return parent::identify($account, $password);
			}
		}
		//验证失败
		return false;
	}
}
